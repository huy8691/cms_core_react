import React from 'react'
import { Layout, Menu, Icon } from 'antd';
import { Redirect, Route, Switch, Link } from 'react-router-dom'
import navigation from './_nav'
import routes from '../../../routes'


const DefaultLayout = () => {
  const { Header, Content, Sider } = Layout;
  return (
      <>
        <Layout>
          <Sider
              style={{
                overflow: 'auto',
                height: '100vh',
                position: 'fixed',
                left: 0,
              }}
          >
            <div className="logo" />
            <Menu theme="dark" mode="inline" defaultSelectedKeys={['4']}>
              {navigation.items.map((route, index) => {
                return (
                    <Menu.Item key={index}>
                      <Link to={route.url}>{route.name}</Link>
                    </Menu.Item>
                )
              })}
            </Menu>
          </Sider>
          <Layout style={{ marginLeft: 200 }}>
            <Header style={{ background: '#fff', padding: 0 }}>Header</Header>
            <Content style={{ margin: '24px 16px 0', overflow: 'initial' }}>
              <div style={{ padding: 24, background: '#fff', textAlign: 'center' }}>
                <main className="main">
                  <Switch>
                    {routes.map((route, idx) => {
                      return (
                          <Route
                              key={idx}
                              path={route.path}
                              exact={route.exact}
                              render={props => <route.component {...props} />}
                          />
                      )
                    })}
                    <Route exact path="/">
                      <Redirect to="/dashboard" />
                    </Route>
                    <Route path="*">
                      <Redirect to="/404" />
                    </Route>
                  </Switch>
                </main>
              </div>
            </Content>
          </Layout>
        </Layout>,
      </>
  )
}



export default DefaultLayout
