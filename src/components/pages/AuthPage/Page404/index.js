import React from 'react'

const Page404 = () => {
  return (
      <>
        <h1 className="title-page">
          <span style={{ color: '#20a8d8' }}> Page 404</span>
        </h1>
      </>
  )
}



export default Page404
