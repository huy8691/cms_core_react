import React from 'react';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom'
import 'antd/dist/antd.css'
import './App.css';

import Login from './components/pages/AuthPage/Login'
import Register from './components/pages/AuthPage/Register'
import ResetPassword from './components/pages/AuthPage/ResetPassword'
import Page404 from './components/pages/AuthPage/Page404'
import Page500 from './components/pages/AuthPage/Page500'
import DefaultLayout from './components/templates/DefaultLayout'


function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Switch>
          <Route
              exact
              path="/login"
              name="Login Page"
              render={props => <Login {...props} />}
          />
          <Route
              exact
              path="/register"
              name="Register Page"
              render={props => <Register {...props} />}
          />
          <Route
              exact
              path="/reset-password"
              name="Reset Password"
              render={props => <ResetPassword {...props} />}
          />
          <Route
              exact
              path="/404"
              name="Page 404"
              render={props => <Page404 {...props} />}
          />
          <Route
              exact
              path="/500"
              name="Page 500"
              render={props => <Page500 {...props} />}
          />
          <Route
              path="/"
              name="Home"
              render={props => <DefaultLayout {...props} />}
          />
          <Route path="*" name="NotMatch"><Redirect to="/404" /></Route>
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
