import React from 'react'


// const Dashboard = React.lazy(() => import('./components/pages/Dashboard'))
// const ListUser = React.lazy(() => import('components/pages/ListUser'))

import Dashboard from './components/pages/Dashboard'
import ListUser from './components/pages/ListUser'

const routes = [
  {
    path: '/dashboard',
    exact: true,
    component: Dashboard,
  },
  {
    path: '/list-user/',
    exact: true,
    component: ListUser,
  },
]

export default routes
